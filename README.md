# amld-materials

## Getting started

1. Install Anaconda (miniconda would suffice)
2. Import the environment from the .yml file:

~~~
conda env create -f amld.yml
~~~

3. Activate the environment
~~~
source activate amld
~~~


## Downloading the dataset
~~~
cd data
curl https://www.dropbox.com/s/ukifanl57wzp7cj/datajson_sample.json?dl=0 -L -o dataset.json
~~~
